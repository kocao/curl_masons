<?php
$member = $_POST['member'];
$lastname = $_POST['lastname'];
$regex_membership = "/<span id=\"lblMessage\"[^>]*>(.*?)<\/span>/";

// Init cURL
$ch = curl_init();
$url = "https://members.indianafreemasons.com/outside/membervalidation/membervalidation.aspx";

// Enable POST protocol
curl_setopt($ch, CURLOPT_URL, $url);

// Set URL
curl_setopt($ch, CURLOPT_POST, 1);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.3) Gecko/20090910 Ubuntu/9.04 (jaunty) Shiretoko/3.5.3");


// Set field search value
// curl_setopt ($ch, CURLOPT_POSTFIELDS, "MembershipNumber=0006415095&LastName=Roy");
curl_setopt ($ch, CURLOPT_POSTFIELDS, "__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=6RPZTLSSsKVYwo6p%2BOvfjUeCrMhwkkiYNcDafrX8NCQ26QbVDgDjrHZQhMdWiQWQNbK1ZutoDjEiXmLtibdssJPdaXXiT867swibV3tsu8nGpvdmXFC7%2FLf5oxZABgVwSF09bA639yJ%2Bv17L0V7FpiitSQFwYJZq%2B4q3g3E2O3CV2WWryLQtCKeE7fX%2Fu4hmaQP9wt1LMREbPe5tSPfAw1qbSChKkcRsMMkccVR%2Fj%2BhGuzt%2FD4xwcRmgLNCQgCEH&__VIEWSTATEGENERATOR=85409CE4&__EVENTVALIDATION=2BqRKIsJoj%2FrXUw7FxtE8YWCFj7MDb1a4JqJaAXx8JnvzC7CfeGw4KUbuZXJaMFI1NejdBB3x%2BpZxFDwZsNrUYDal5Uf6CEGs5roiFdniojdW2lmn1GVx6Rk0ZTCRlVWPbm1H0iX8zdg9HNeDgAyRu4zacutBYoVFLge5SI6mIw%3D&MembershipNumber=$member&LastName=$lastname&cmdValidateMembershipNumber=Validate+Member+Card");

// Execute
$content = curl_exec($ch);

// Get result with regex
preg_match($regex_membership, $content, $match);

echo $match[1];

// close curl
curl_close ($ch);