function function1(fieldmember){
  let member = document.getElementById(fieldmember).value

  var ajax = new XMLHttpRequest();

  // Seta tipo de requisição: Post e a URL da API
  ajax.open("POST", "function1.php", true);
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  // Seta paramêtros da requisição e envia a requisição
  ajax.send("member=" + member);

  // Cria um evento para receber o retorno.
  ajax.onreadystatechange = function() {

  // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
  if (ajax.readyState == 4 && ajax.status == 200) {

      var data = ajax.responseText;
      document.getElementById ("result1").textContent = data;
  }
  }

}

function function2(fieldmember, fieldlastname){
  let member = document.getElementById (fieldmember).value
  let lastname = document.getElementById (fieldlastname).value

  var ajax = new XMLHttpRequest();

  // Seta tipo de requisição: Post e a URL da API
  ajax.open("POST", "function2.php", true);
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  // Seta paramêtros da requisição e envia a requisição
  ajax.send("member=" + member + "&lastname=" + lastname );

  // Cria um evento para receber o retorno.
  ajax.onreadystatechange = function() {

  // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
  if (ajax.readyState == 4 && ajax.status == 200) {

      var data = ajax.responseText;

      document.getElementById ("result2").textContent = data;

  }
  }

}
