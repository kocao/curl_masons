<?php
$member = $_POST['member'];
$regex_membership = "/<span[^>]*>(.*?)<\/span>/";

// Init cURL
$ch = curl_init();
$url = "https://wa.gvsoftware.com/public_member_verification/";

// Enable POST protocol
curl_setopt($ch, CURLOPT_URL, $url);

// Set URL
curl_setopt($ch, CURLOPT_POST, 1);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// Set field search value
curl_setopt ($ch, CURLOPT_POSTFIELDS, "member[id]=$member");

// Execute
$content = curl_exec($ch);

// Get result with regex
preg_match($regex_membership, $content, $match);

// echo result
echo trim(strip_tags($match[1]));

// close curl
curl_close ($ch);